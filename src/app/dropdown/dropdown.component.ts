import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-dropdown",
  templateUrl: "./dropdown.component.html",
  styleUrls: ["./dropdown.component.css"],
})
export class DropdownComponent implements OnInit {
  @Input() option;
  @Input() selOpt;
  @Output() selOption = new EventEmitter<string>();
  constructor() {}

  ngOnInit() {}

  emitChange(s1) {
    this.selOption.emit(s1);
  }
}
