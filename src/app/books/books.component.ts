import { NetService } from "./../net.service";
import { ActivatedRoute, Router, ParamMap } from "@angular/router";
import { Component, OnInit } from "@angular/core";

@Component({
  selector: "app-books",
  templateUrl: "./books.component.html",
  styleUrls: ["./books.component.css"],
})
export class BooksComponent implements OnInit {
  url = "https://www.googleapis.com/books/v1/volumes?q=";
  lang = [
    { lang: "English", val: "en" },
    { lang: "French", val: "fr" },
    { lang: "Hindi", val: "hi" },
  ];
  filterArray = [
    { fil: "Full Volume", val: "full-volume" },
    { fil: "Free Google e-books", val: "free-ebooks" },
    { fil: "Paid Google e-books", val: "paid-ebooks" },
  ];
  books;
  searchtxt = "";
  startIndex = "0";
  endIndex = 8;
  maxResults = "8";
  structureLang = null;
  filterStructure = null;
  language = "";
  filtered = "";
  n: number = 1;
  constructor(
    private netservice: NetService,
    private route: ActivatedRoute,
    private router: Router
  ) {}
  ngOnInit() {
    this.route.queryParamMap.subscribe((param) => {
      this.searchtxt = param.get("searchtxt");
      this.startIndex = param.get("startIndex");
      this.maxResults = param.get("maxResults");
      this.language = param.get("langRestrict");
      this.filtered = param.get("filter");
      // console.log(this.searchtxt);
      if (this.language && this.filtered) {
        this.netservice
          .getData(
            this.url +
              this.searchtxt +
              "&startIndex=" +
              this.startIndex +
              "&maxResults=" +
              this.maxResults +
              "&langRestrict=" +
              this.language +
              "&filter=" +
              this.filtered
          )
          .subscribe((resp) => {
            this.books = resp;
            console.log(this.books.items);
            this.makeStructure();
          });
      } else if (this.language) {
        this.netservice
          .getData(
            this.url +
              this.searchtxt +
              "&startIndex=" +
              this.startIndex +
              "&maxResults=" +
              this.maxResults +
              "&langRestrict=" +
              this.language
          )
          .subscribe((resp) => {
            this.books = resp;
            console.log(this.books.items);
            this.makeStructure();
          });
      } else if (this.filtered) {
        this.netservice
          .getData(
            this.url +
              this.searchtxt +
              "&startIndex=" +
              this.startIndex +
              "&maxResults=" +
              this.maxResults +
              "&filter=" +
              this.filtered
          )
          .subscribe((resp) => {
            this.books = resp;
            console.log(this.books.items);
            this.makeStructure();
          });
      } else {
        console.log(this.startIndex);
        if (this.startIndex == "0") {
          this.startIndex = "0";
          this.endIndex = 8;
          this.maxResults = "8";
          this.n = 1;
        }
        this.netservice
          .getData(
            this.url +
              this.searchtxt +
              "&startIndex=" +
              this.startIndex +
              "&maxResults=" +
              this.maxResults
          )
          .subscribe((resp) => {
            this.books = resp;
            console.log(this.books.items);
            this.makeStructure();
          });
      }
    });
  }
  makeStructure() {
    this.structureLang = {
      language: this.lang,
      selected: this.language ? this.language : "",
    };
    this.filterStructure = {
      filters: this.filterArray,
      selected: this.filtered ? this.filtered : "",
    };
  }
  optChange() {
    this.startIndex = "0";
    this.endIndex = 8;
    this.maxResults = "8";
    this.n = 1;
    let path = "books";
    let qparam = {};
    if (this.structureLang.selected) {
      console.log(this.structureLang.selected);
      this.language = this.structureLang.selected;
      qparam["searchtxt"] = this.searchtxt;
      qparam["startIndex"] = this.startIndex;
      qparam["maxResults"] = this.maxResults;
      qparam["langRestrict"] = this.language;
    }
    if (this.filterStructure.selected) {
      console.log(this.filterStructure.selected);
      this.filtered = this.filterStructure.selected;
      qparam["searchtxt"] = this.searchtxt;
      qparam["startIndex"] = this.startIndex;
      qparam["maxResults"] = this.maxResults;
      qparam["filter"] = this.filtered;
    }
    this.router.navigate([path], { queryParams: qparam });
  }
  next() {
    this.n += +this.maxResults;
    this.startIndex = "" + this.n;
    this.endIndex += 8;
    let path = "books";
    let qparam = {};
    if (this.searchtxt) {
      qparam["searchtxt"] = this.searchtxt;
      qparam["startIndex"] = this.startIndex;
      qparam["maxResults"] = this.maxResults;
      qparam["langRestrict"] = this.language;
      qparam["filter"] = this.filtered;
    }
    this.router.navigate([path], { queryParams: qparam });
  }
  previous() {
    this.n -= +this.maxResults;
    this.startIndex = "" + this.n;
    this.endIndex -= 8;
    let path = "books";
    let qparam = {};
    if (this.searchtxt) {
      qparam["searchtxt"] = this.searchtxt;
      qparam["startIndex"] = this.startIndex;
      qparam["maxResults"] = this.maxResults;
      qparam["langRestrict"] = this.language;
      qparam["filter"] = this.filtered;
    }
    this.router.navigate([path], { queryParams: qparam });
  }
}
