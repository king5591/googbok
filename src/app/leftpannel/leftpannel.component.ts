import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-leftpannel",
  templateUrl: "./leftpannel.component.html",
  styleUrls: ["./leftpannel.component.css"],
})
export class LeftpannelComponent implements OnInit {
  @Input() language;
  @Input() filter;
  @Output() selOpt = new EventEmitter();
  constructor() {}

  ngOnInit() {}
  emitChange() {
    this.selOpt.emit();
  }
}
