import { ActivatedRoute, Router, ParamMap } from "@angular/router";
import { NetService } from "./net.service";
import { Component } from "@angular/core";
@Component({
  selector: "app-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.css"],
})
export class AppComponent {
  options = [
    "Harry Potter Books",
    "Books by Agatha Christie",
    "Books by Premchand",
    "Love Stories by Jane",
    "Biography on Lincoln",
  ];
  s = true;
  searchtxt: string = "";
  selOpt: string = "";
  startIndex = "0";
  maxResults = "8";
  constructor(
    private netservice: NetService,
    private route: ActivatedRoute,
    private router: Router
  ) {}
  ngOnInit() {}
  search(s1) {
    this.s = false;
    this.searchtxt = s1;
    let path = "/books";
    let qparam = {};
    qparam["searchtxt"] = this.searchtxt;
    qparam["startIndex"] = this.startIndex;
    qparam["maxResults"] = this.maxResults;
    this.router.navigate([path], { queryParams: qparam });
  }
  optChange(s1) {
    this.s = false;
    this.searchtxt = s1;
    let path = "/books";
    let qparam = {};
    qparam["searchtxt"] = this.searchtxt;
    qparam["startIndex"] = this.startIndex;
    qparam["maxResults"] = this.maxResults;
    this.router.navigate([path], { queryParams: qparam });
  }
}
