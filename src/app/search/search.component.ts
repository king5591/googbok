import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";

@Component({
  selector: "app-search",
  templateUrl: "./search.component.html",
  styleUrls: ["./search.component.css"],
})
export class SearchComponent implements OnInit {
  @Input() searchtxt;
  @Output() text = new EventEmitter();
  constructor() {}

  ngOnInit() {}

  emitSearch() {
    this.text.emit(this.searchtxt);
  }
}
