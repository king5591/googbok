import { HttpClientModule } from "@angular/common/http";
import { NetService } from "./net.service";
import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { FormsModule } from "@angular/forms";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { BooksComponent } from "./books/books.component";
import { DropdownComponent } from './dropdown/dropdown.component';
import { LeftpannelComponent } from './leftpannel/leftpannel.component';
import { SearchComponent } from './search/search.component';

@NgModule({
  declarations: [AppComponent, BooksComponent, DropdownComponent, LeftpannelComponent, SearchComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    HttpClientModule,
  ],
  providers: [NetService],
  bootstrap: [AppComponent],
})
export class AppModule {}
